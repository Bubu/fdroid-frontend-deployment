<!--
SPDX-FileCopyrightText: 2020 Michael Pöhn <michael.poehn@fsfe.org>
SPDX-License-Identifier: CC0-1.0
-->

# F-Droid Frontend Deployment

Deployment scripts for f-droid.org frontend servers.

```
ansible-galaxy install -f -r requirements.yml -p .galaxy
ansible-playbook -i inventory webserver.yml
```
